#!/usr/bin/env python

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-r", "--rmax", action="store",dest="rmax",help="maximum radius to plot (default plot all)")
parser.add_option("-f", "--field", action="store",dest="field",help="field to plot (default 'rho')")
parser.add_option("-m", "--min", action="store",dest="min",help="min value for colorbar")
parser.add_option("-x", "--max", action="store",dest="max",help="max value for colorbar")
parser.add_option("-l", "--log", action="store_true",dest="log",help="plot log rho (default False)")
parser.add_option("-c", "--crash", action="store_true",dest="crash",help="plot crash.dat only (default False)")
parser.add_option("-a", "--start", action="store",dest="start",default=0,help="starting ivar")
parser.add_option("-b", "--stop", action="store",dest="stop",default=0,help="stopping ivar")
parser.add_option("-t", "--temp", action="store",dest="temp",help="temp instead of uz (--temp=cs0)")
parser.add_option("-g", "--gonuts", action="store_true",dest="gonuts",help="plot every iphi, itheta (default False, but automatic for --crash)")
(options, args) = parser.parse_args()
if options.crash:
 options.gonuts = True

from sys import argv, stdout
from os import system
from matplotlib import use
use("agg")
from pencil import read_var
from numpy import newaxis, cos, sin, linspace, min, max, size, zeros, ones_like, ones, log, meshgrid, newaxis, isinf, nan, arange, exp, log10
from scipy import nanmax,nanmin
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from mpl_toolkits.axes_grid1 import make_axes_locatable

axisbg = 'black'
figsize = (15,10) #(w,h)
dpi = 400 #200
plt.rcParams['figure.dpi'] = dpi
plt.rcParams['savefig.dpi'] = dpi
lw = 0.5
wspace = 0.2
hspace = 0.15
top = 0.95
left = 0.05
right = 0.95
lncolor = 'white'

if options.crash:
 files = ['crash.dat']
else:
 files = arange(int(options.start),int(options.stop)+1)

for filename in files:
 if options.crash:
  ff = read_var(varfile=filename,quiet=True)
 else:
  ff = read_var(ivar=filename,quiet=True)

 rho = ff.rho
 ux = ff.ux
 uy = ff.uy
 if options.temp==None:
  uz = ff.uz
 else:
  cp=1.0
  gamma=1.4
  cv1=gamma*cp
  cs0=float(options.temp)
  TT0=(cs0**2.)/(gamma-1)
  lnTT0=log(TT0)
  rho0=1.0
  lnrho0=log(rho0)
  lnrho=log(ff.rho)
  lnTT=lnTT0+cv1*ff.ss+(gamma-1)*(lnrho-lnrho0)
  uz = exp(lnTT)

 rho[isinf(rho)] = rho[isinf(rho)]*nan
 ux[isinf(ux)] = ux[isinf(ux)]*nan
 uy[isinf(uy)] = uy[isinf(uy)]*nan
 uz[isinf(uz)] = uz[isinf(uz)]*nan
 
 if options.log:
  rho=log10(rho)
 
 rhomin=nanmin(rho)
 rhomax=nanmax(rho)
 uxmin=nanmin(ux)
 uxmax=nanmax(ux)
 uymin=nanmin(uy)
 uymax=nanmax(uy)
 uzmin=nanmin(uz)
 uzmax=nanmax(uz)
 
 rhoLevels=linspace(rhomin,rhomax,200)
 uxLevels=linspace(uxmin,uxmax,200)
 uyLevels=linspace(uymin,uymax,200)
 uzLevels=linspace(uzmin,uzmax,200)
 
 radius=ff.x
 theta=ff.y
 phi=ff.z
 nradius=size(radius)
 ntheta=size(theta)
 nphi=size(phi)
# rr_xz,pp_xz=meshgrid(radius,phi)
# rr_xy,tt_xy=meshgrid(radius,theta)
# xx_xz=rr_xz*cos(pp_xz)
# yy_xz=rr_xz*sin(pp_xz)
# xx_xy=rr_xy*sin(tt_xy)
# yy_xy=rr_xy*cos(tt_xy)
 dummyrho = ones_like(rho)
 rrr = radius[newaxis,newaxis,:]*dummyrho
 ttt = theta[newaxis,:,newaxis]*dummyrho
 ppp = phi[:,newaxis,newaxis]*dummyrho
 xxx = rrr*sin(ttt)*cos(ppp)
 yyy = rrr*sin(ttt)*sin(ppp)
 zzz = rrr*cos(ttt)

 zf_phi = 4 #len(str(nphi))
 zf_theta = 4 #len(str(ntheta))

 rmin = min(radius)
 rmax = max(radius)

 tmin = min(theta)
 tmax = max(theta)
 x_rmin = rmin*sin(theta)
 y_rmin = rmin*cos(theta)
 x_rmax = rmax*sin(theta)
 y_rmax = rmax*cos(theta)

 if options.gonuts:
  phiIndices = xrange(nphi)
  thetaIndices = xrange(ntheta)
 else:
  phiIndices = [0] #,nphi/2]
  thetaIndices =  [] #[0,ntheta/2,ntheta-1]

 for i in phiIndices:
  fig, axes = plt.subplots(nrows=2, ncols=2,subplot_kw=dict(aspect=1,axisbg=axisbg),figsize=figsize,dpi=dpi)
  rr,tt = meshgrid(radius,theta)
  xx = rr*sin(tt)
  yy = rr*cos(tt)
  Contour_sideA_rho=axes[0,0].pcolor(xx,yy,rho[i,:,:]) #,levels=rhoLevels) #,cmap=plt.cm.hot)
  axes[0,0].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[0,0].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[0,0].plot([x_rmin[0],x_rmax[0]],[y_rmin[0],y_rmax[0]],color=lncolor,lw=lw)
  axes[0,0].plot([x_rmin[-1],x_rmax[-1]],[y_rmin[-1],y_rmax[-1]],color=lncolor,lw=lw)
  axes[0,0].set_title("rho",fontsize=10)
  #divider00 = make_axes_locatable(axes[0,0])
  #cax00 = divider00.append_axes("right", size="20%", pad=0.05)
  #cax00 = inset_axes(axes[0,0],width="5%",height="50%",loc=3,bbox_to_anchor=(1.05, 0., 1, 1),bbox_transform=axes[0,0].transAxes,borderpad=0)
  fig.colorbar(Contour_sideA_rho,ax=axes[0,0])
  Contour_sideA_ux=axes[0,1].pcolor(xx,yy,ux[i,:,:]) #,levels=uxLevels) #,cmap=plt.cm.hot)
  axes[0,1].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[0,1].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[0,1].plot([x_rmin[0],x_rmax[0]],[y_rmin[0],y_rmax[0]],color=lncolor,lw=lw)
  axes[0,1].plot([x_rmin[-1],x_rmax[-1]],[y_rmin[-1],y_rmax[-1]],color=lncolor,lw=lw)
  axes[0,1].set_title("ux",fontsize=10)
  #cax01 = inset_axes(axes[0,1],width="5%",height="50%",loc=3,bbox_to_anchor=(1.05, 0., 1, 1),bbox_transform=axes[0,1].transAxes,borderpad=0)
  #divider01 = make_axes_locatable(axes[0,1])
  #cax01 = divider01.append_axes("right", size="20%", pad=0.05)
  fig.colorbar(Contour_sideA_ux,ax=axes[0,1])
  Contour_sideA_uy=axes[1,0].pcolor(xx,yy,uy[i,:,:]) #,levels=uyLevels) #,cmap=plt.cm.hot)
  axes[1,0].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[1,0].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[1,0].plot([x_rmin[0],x_rmax[0]],[y_rmin[0],y_rmax[0]],color=lncolor,lw=lw)
  axes[1,0].plot([x_rmin[-1],x_rmax[-1]],[y_rmin[-1],y_rmax[-1]],color=lncolor,lw=lw)
  axes[1,0].set_title("uy",fontsize=10)
  #cax10 = inset_axes(axes[1,0],width="5%",height="50%",loc=3,bbox_to_anchor=(1.05, 0., 1, 1),bbox_transform=axes[1,0].transAxes,borderpad=0)
  #divider10 = make_axes_locatable(axes[1,0])
  #cax10 = divider10.append_axes("right", size="20%", pad=0.05)
  fig.colorbar(Contour_sideA_uy,ax=axes[1,0])
  Contour_sideA_uz=axes[1,1].pcolor(xx,yy,uz[i,:,:]) #,levels=uzLevels) #,cmap=plt.cm.hot)
  axes[1,1].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[1,1].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[1,1].plot([x_rmin[0],x_rmax[0]],[y_rmin[0],y_rmax[0]],color=lncolor,lw=lw)
  axes[1,1].plot([x_rmin[-1],x_rmax[-1]],[y_rmin[-1],y_rmax[-1]],color=lncolor,lw=lw)
  if options.temp==None:
   axes[1,1].set_title("uz",fontsize=10)
  else:
   axes[1,1].set_title("temp",fontsize=10)
  #divider11 = make_axes_locatable(axes[1,1])
  #cax11 = divider11.append_axes("right", size="20%", pad=0.05)
  #cax11 = inset_axes(axes[1,1],width="5%",height="50%",loc=3,bbox_to_anchor=(1.05, 0., 1, 1),bbox_transform=axes[1,1].transAxes,borderpad=0)
  fig.colorbar(Contour_sideA_uz,ax=axes[1,1])
  title="time: "+str(ff.t)+", phi=%s"%str(phi[i])
  #fig.subplots_adjust(wspace=wspace,top=top,hspace=hspace,left=left,right=right)
  fig.suptitle(title)
  outname="sph."+str(filename)+".iphi."+str(i).zfill(zf_phi)+".png"
  fig.savefig(outname)
  plt.close(fig)
 
 x_rmin = rmin*cos(phi)
 y_rmin = rmin*sin(phi)
 x_rmax = rmax*cos(phi)
 y_rmax = rmax*sin(phi)

 for i in thetaIndices:
  fig, axes = plt.subplots(nrows=2, ncols=2,subplot_kw=dict(aspect=1,axisbg=axisbg),figsize=figsize,dpi=dpi)
  Contour_midplane_rho=axes[0,0].pcolor(xxx[:,i,:],yyy[:,i,:],rho[:,i,:]) #,levels=rhoLevels) #,cmap=plt.cm.hot)
  axes[0,0].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[0,0].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[0,0].set_title("rho",fontsize=10)
  axes[0,0].set_xlim(-rmax,rmax)
  axes[0,0].set_ylim(-rmax,rmax)
  fig.colorbar(Contour_midplane_rho,ax=axes[0,0])
  Contour_midplane_ux=axes[0,1].pcolor(xxx[:,i,:],yyy[:,i,:],ux[:,i,:]) #,levels=uxLevels) #,cmap=plt.cm.hot)
  axes[0,1].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[0,1].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[0,1].set_title("ux",fontsize=10)
  axes[0,1].set_xlim(-rmax,rmax)
  axes[0,1].set_ylim(-rmax,rmax)
  fig.colorbar(Contour_midplane_ux,ax=axes[0,1])
  Contour_midplane_uy=axes[1,0].pcolor(xxx[:,i,:],yyy[:,i,:],uy[:,i,:]) #,levels=uyLevels) #,cmap=plt.cm.hot)
  axes[1,0].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[1,0].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  axes[1,0].set_title("uy",fontsize=10)
  axes[1,0].set_xlim(-rmax,rmax)
  axes[1,0].set_ylim(-rmax,rmax)
  fig.colorbar(Contour_midplane_uy,ax=axes[1,0])
  Contour_midplane_uz=axes[1,1].pcolor(xxx[:,i,:],yyy[:,i,:],uz[:,i,:]) #,levels=uzLevels) #,cmap=plt.cm.hot)
  axes[1,1].plot(x_rmin,y_rmin,color=lncolor,lw=lw)
  axes[1,1].plot(x_rmax,y_rmax,color=lncolor,lw=lw)
  if options.temp==None:
   axes[1,1].set_title("uz",fontsize=10)
  else:
   axes[1,1].set_title("temp",fontsize=10)
  axes[1,1].set_xlim(-rmax,rmax)
  axes[1,1].set_ylim(-rmax,rmax)
  fig.colorbar(Contour_midplane_uz,ax=axes[1,1])
  title="time: "+str(ff.t)+", theta=%s"%str(theta[i])
  fig.subplots_adjust(wspace=wspace,top=top,hspace=hspace,left=left,right=right)
  fig.suptitle(title)
  outname="sph."+str(filename)+".itheta."+str(i).zfill(zf_theta)+".png"
  fig.savefig(outname,dpi=dpi)
  plt.close(fig)

 print
