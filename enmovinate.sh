#!/bin/bash
# Make an animation out of all *.png files in current directory.
# Optional argument is movie extension/format (default "mp4")

nfile=$(ls [X-Z][X-Z]slice*.png | wc -l)

if [ -z $1 ]
 then
  ext='mpeg'
else
 ext=$1
fi

#convert -verbose -quality 1000 $(ls *png | sort -V) movie.$ext

filez=$(ls *png | grep 0000 | sed 's/0000/\%4d/')
ffmpeg -framerate 5 -i $filez -r 20 -g 20 -q 1 animation.$ext
