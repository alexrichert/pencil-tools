#!/usr/bin/python

cart = True

import pencil as pc
from numpy import array, cos, int64, linspace, loadtxt, meshgrid, pi, sin
import matplotlib.pyplot as plt

r = loadtxt("regions.dat")

r = int64(r-1)

ff = pc.read_var(quiet=True,trimall=True)

f = plt.figure()
ax = f.add_subplot(111,aspect=1)

ff.x = ff.x[::-1] ; ff.y = ff.y[::-1]

rr, pp = meshgrid(ff.x, linspace(-pi,pi,len(ff.z)))
xx = rr*cos(pp) ; yy = rr*sin(pp)

if cart: ax.pcolor(xx,yy,ff.potself[:,0,:])
else: ax.pcolor(rr,pp,ff.potself[:,0,:])

n = 20

for e in r:
 r1, t1 = linspace(ff.x[e[3]]+0.5*ff.dx,ff.x[e[3]]+0.5*ff.dx,20),linspace(ff.z[e[7]]-0.5*ff.dz,ff.z[e[8]]+0.5*ff.dz,20)
 r2, t2 = linspace(ff.x[e[4]]-0.5*ff.dx,ff.x[e[4]]-0.5*ff.dx,20),linspace(ff.z[e[7]]-0.5*ff.dz,ff.z[e[8]]+0.5*ff.dz,20)
 r3, t3 = linspace(ff.x[e[3]]+0.5*ff.dx,ff.x[e[4]]-0.5*ff.dx,20),linspace(ff.z[e[7]]-0.5*ff.dz,ff.z[e[7]]-0.5*ff.dz,20)
 r4, t4 = linspace(ff.x[e[3]]+0.5*ff.dx,ff.x[e[4]]-0.5*ff.dx,20),linspace(ff.z[e[8]]+0.5*ff.dz,ff.z[e[8]]+0.5*ff.dz,20)
 if cart:
  #r1, t1 = linspace(ff.x[e[3]],ff.x[e[3]],n),linspace(ff.z[e[7]],ff.z[e[8]],n)
  #r2, t2 = linspace(ff.x[e[4]],ff.x[e[4]],n),linspace(ff.z[e[7]],ff.z[e[8]],n)
  #r3, t3 = linspace(ff.x[e[3]],ff.x[e[4]],n),linspace(ff.z[e[7]],ff.z[e[7]],n)
  #r4, t4 = linspace(ff.x[e[3]],ff.x[e[4]],n),linspace(ff.z[e[8]],ff.z[e[8]],n)
  x1, y1 = r1*cos(t1) , r1*sin(t1) ; x2, y2 = r2*cos(t2) , r2*sin(t2)
  x3, y3 = r3*cos(t3) , r3*sin(t3) ; x4, y4 = r4*cos(t4) , r4*sin(t4)
  plt.plot(x1,y1,color='black') ; plt.plot(x2,y2,color='black')
  plt.plot(x3,y3,color='black') ; plt.plot(x4,y4,color='black')
 else:
  plt.plot(r1,t1,color='black')
  plt.plot(r2,t2,color='black')
  plt.plot(r3,t3,color='black')
  plt.plot(r4,t4,color='black')
  #plt.plot([ff.x[e[3]],ff.x[e[3]]],[ff.z[e[7]],ff.z[e[8]]],color='black')
  #plt.plot([ff.x[e[4]],ff.x[e[4]]],[ff.z[e[7]],ff.z[e[8]]],color='black')
  #plt.plot([ff.x[e[3]],ff.x[e[4]]],[ff.z[e[7]],ff.z[e[7]]],color='black')
  #plt.plot([ff.x[e[3]],ff.x[e[4]]],[ff.z[e[8]],ff.z[e[8]]],color='black')

r, t = ff.x[15],ff.z[31]
if cart:
 x = r*cos(t)
 y = r*sin(t)
 plt.scatter(x,y)
else:
 plt.scatter(r,t)
 

plt.show()
