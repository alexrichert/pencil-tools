#!/usr/bin/env python

import pencil as pc
from sys import argv
from numpy import ceil, cos, exp, log, log10, meshgrid, max, min, pi, shape, sin, sqrt
from matplotlib import use
use("pdf")
import matplotlib.pyplot as plt
#plt.ioff()
plt.rcdefaults()
#plt.rcParams["backend"] = "cairo"
try: plt.rcParams["font.family"] = "Gill Sans MT Pro"
except: plt.rcParams["font.family"] = "Gill Sans"
try: plt.rcParams["mathtext.fontset"] = "Gill Sans MT Pro"
except: plt.rcParams["mathtext.fontset"] = "stixsans"

varfile = argv[1]

def getdims(n):
 if n==3: return (1,3)
 ncell = ceil(sqrt(n))**2.0
 x = int(sqrt(ncell))
 if x*(x-1)>=n:
  return (x-1,x)
 else:
  return (x,x)

ff = pc.read_var(quiet=True,trimall=True,varfile=varfile)
pp = pc.read_param(quiet=True)

varnames = argv[2:]
Nvar = len(varnames)
do_log = []
for i in xrange(Nvar):
 if varnames[i][0:3]=="log":
  varnames[i] = varnames[i][4:-1]
  do_log += [True]
 else: do_log += [False]

if pp.coord_system=="spherical":
 rr, tt = meshgrid(ff.x,ff.z)
 xx = rr*cos(tt); yy = rr*sin(tt)
if pp.coord_system=="cylindric":
 rr, tt = meshgrid(ff.x,ff.y)
 xx = rr*cos(tt); yy = rr*sin(tt)

dims = getdims(Nvar)
f, axes = plt.subplots(nrows=dims[0],ncols=dims[1],subplot_kw=dict(aspect=1),dpi=300)

for i in xrange(Nvar):
 if varnames[i] != "temp": exec("var = ff.%s"%varnames[i])
 #if do_log[i]: var=log10(var)
 elif pp.ldensity_nolog: var = pp.cs0**2.0*exp(pp.gamma*ff.ss+(pp.gamma-1)*log(ff.rho/pp.rho0))
 else: var = pp.cs0**2.0*exp(pp.gamma*ff.ss+(pp.gamma-1)*log(ff.lnrho/pp.lnrho0))/(pp.gamma-1.0)
 if len(shape(axes))==0:
  ax = axes
 elif len(shape(axes))==1:
  ax = axes[i]
 else:
  x,y = i%dims[0],float(i)/dims[0]
  ax = axes[x,y]
 if pp.coord_system=="spherical": ax.pcolor(xx,yy,var[:,len(ff.y)/2,:],rasterized=True) #if spherical
 if pp.coord_system=="cylindric": ax.pcolor(xx,yy,var[len(ff.z)/2,:,:],rasterized=True) #if spherical
 ax.set_title(varnames[i])
 ax.set_xlim(min(xx),max(xx))
 ax.set_ylim(min(yy),max(yy))

f.suptitle("%s orbits"%round(ff.t/(2*pi),4))
basename = varfile+"_"+"_".join(varnames)
plt.savefig("varplot_%s.pdf"%(basename),bbox_inches='tight',dpi=300)
