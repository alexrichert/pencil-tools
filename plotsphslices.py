#!/usr/bin/env python

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-s", "--start", action="store",dest="starttime",help="starting time step (default 0)")
parser.add_option("-n", "--ntime", action="store",dest="ntime",help="number of time steps (default unlimited)")
parser.add_option("-r", "--rlim", action="store",dest="rlim",help="maximum radius to plot (default plot all)")
parser.add_option("-f", "--field", action="store",dest="field",help="field to plot (default 'rho')")
parser.add_option("-l", "--last", action="store_true",dest="last",help="plot last timestep only (default False)")
parser.add_option("-m", "--min", action="store",dest="min",help="min value for colorbar")
parser.add_option("-x", "--max", action="store",dest="max",help="max value for colorbar")
parser.add_option("-g", "--log", action="store_true",dest="log",help="plot log values (default False)")
parser.add_option("-q", "--sqrt", action="store_true",dest="sqrt",help="plot sqrt values (default False)")
(options, args) = parser.parse_args()

from sys import argv, stdout
from os import system
from matplotlib import use
use("agg")
from pencil import read_grid, read_slices
from numpy import newaxis, cos, sin, linspace, min, max, size, zeros, ones_like, ones, log, meshgrid, log10, sqrt, percentile
import matplotlib.pyplot as plt

plt.rcParams['text.usetex']=False

stdout.write("\rReading grid file...   ")
stdout.flush()
grid=read_grid(quiet=True)

if (type(options.field)!=type(None)):
 field=options.field
else:
 field='rho'

slice_xz=read_slices(field=field,extension='xz')
slice_xy=read_slices(field=field,extension='xy')

xzdata=slice_xz[0]
xydata=slice_xy[0]

if options.log:
 xzdata=log10(xzdata)
 xydata=log10(xydata)
if options.sqrt:
 xzdata=sqrt(xzdata)
 xydata=sqrt(xydata)

timestamps=slice_xz[1]
timemax=size(timestamps)
starttime=0

if (options.starttime>0):
 starttime=int(options.starttime)
if (options.ntime>0):
 timemax=starttime+int(options.ntime)

if (type(options.min)!=type(None)):
 levelmin=float(options.min)
else:
 levelmin=min((min(xzdata),min(xydata)))
if (type(options.max)!=type(None)):
 levelmax=float(options.max)
else:
 levelmax=max((max(xzdata),max(xydata)))
if options.field=="rhop":
 levelmin=percentile(xzdata,1)
 levelmax=percentile(xzdata,99)

myLevels=linspace(levelmin,levelmax,200)

radius=grid.x[3:-3]
theta=grid.y[3:-3]
phi=grid.z[3:-3]
nradius=size(radius)
ntheta=size(theta)
nphi=size(phi)
rr_xz,pp_xz=meshgrid(radius,phi)
rr_xy,tt_xy=meshgrid(radius,theta)
xx_xz=rr_xz*cos(pp_xz)
yy_xz=rr_xz*sin(pp_xz)
xx_xy=rr_xy*sin(tt_xy)
yy_xy=rr_xy*cos(tt_xy)

if options.last:
 starttime=timemax-1

for time in xrange(starttime,timemax):
 stdout.write("\rPlotting timestep %s (%s/%s)"%(str(time),str(time-starttime+1),str(timemax-starttime)))
 stdout.flush()

 fig, axes = plt.subplots(nrows=1, ncols=2,subplot_kw=dict(aspect=1))
 myContour_xz=axes[0].contourf(xx_xz,yy_xz,xzdata[time],levels=myLevels) #,cmap=plt.cm.hot)
 myContour_xy=axes[1].contourf(xx_xy,yy_xy,xydata[time],levels=myLevels) #,cmap=plt.cm.hot)
 rlimname=""
# if (options.rlim>0):
#  rlim=float(options.rlim)
#  plt.xlim(-rlim,rlim)
#  plt.ylim(-rlim,rlim)
#  rlimname=str(rlim)+"."

 cb=plt.colorbar(myContour_xz)
 if options.log:
  cb.set_label("log(%s)"%field)
 elif options.sqrt:
  cb.set_label("sqrt(%s)"%field)
 else:
  cb.set_label(field)

 title="timestep:\n"+str(timestamps[time])
 fig.text(0.5,0.9,title,horizontalalignment='center')
 fig.subplots_adjust(left=0.05,bottom=0.03,top=0.97)
 outname="sphslice."+field+"."+rlimname+str(time).zfill(4)+".png"
 fig.savefig(outname)
 plt.close(fig)

print
