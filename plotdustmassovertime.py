#!/usr/bin/env python

import os
import pencil as pc
import numpy as np
import matplotlib.pyplot as plt

ntimes = 0
filelist = os.listdir("./data/proc0/")
for file in filelist:
 if "PVAR" in file: ntimes += 1
times = np.arange(ntimes)
totaldustmass = []

params = pc.read_param(quiet=True,param2=True)

for i in times:
 pp = pc.read_pvar(varfile="PVAR"+str(i))
 totaldustmass += [(4.0/3.0)*np.pi*sum(pp.npswarm*pp.ap**3.0)*params.rhopmat]

f = plt.figure()
ax = f.add_subplot(111)
p = ax.plot(times*params.dsnap/(2.0*np.pi),totaldustmass,lw=5)
plt.xlabel("# orbits at birth ring")
plt.ylabel("disk mass (arbitrary units)")
plt.show()
