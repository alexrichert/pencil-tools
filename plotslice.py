#!/usr/bin/env python

from optparse import OptionParser
parser = OptionParser()
parser.add_option("-s", "--start", action="store",dest="starttime",help="starting time step (default 0)")
parser.add_option("-n", "--ntime", action="store",dest="ntime",help="number of time steps (default unlimited)")
parser.add_option("-r", "--rlim", action="store",dest="rlim",help="maximum radius to plot (default plot all)")
parser.add_option("-f", "--field", action="store",dest="field",help="field to plot (default 'rho')")
parser.add_option("-l", "--last", action="store_true",dest="last",help="plot last timestep only (default False)")
parser.add_option("-m", "--min", action="store",dest="min",help="min value for colorbar")
parser.add_option("-x", "--max", action="store",dest="max",help="max value for colorbar")
parser.add_option("-g", "--log", action="store_true",dest="log",help="plot log10 values (default False)")
parser.add_option("-e", "--ext", action="store",dest="extension",help="slice file extension (default 'xz')")
parser.add_option("-v", "--video", action="store_true",dest="video",help="prepare slice files (default False)")
parser.add_option("-d", "--dpi", action="store",dest="dpi",help="plot DPI (default 200)",default=200)
(options, args) = parser.parse_args()

from sys import argv, stdout
from os import system
from pencil import read_grid, read_slices, read_param
from numpy import newaxis, cos, sin, linspace, min, max, size, zeros, ones_like, ones, meshgrid, log10
import numpy as np
from time import time
from matplotlib import use
use("agg")
import matplotlib.pyplot as plt
import multiprocessing as mp

outdir = "./animation_files/"
system("mkdir -p %s"%outdir)

params = read_param(quiet=True)
coords = params.coord_system

if options.video:
 system("src/read_all_videofiles.x")

stdout.write("Reading slices...   ")
stdout.flush()

stdout.write("\rReading grid file...   ")
stdout.flush()
grid=read_grid(quiet=True)

if (type(options.field)!=type(None)):
 field=options.field
else:
 field='rho'



if (type(options.extension)!=type(None)):
 extension=options.extension
elif coords=='cartesian' or coords=="cylindric":
 extension='xy'
elif coords=='spherical':
 extension='xz'


slice=read_slices(field=field,extension=extension)

stdout.write("\rPreparing grid...   ")
stdout.flush()
thedata=slice[0]
if options.log:
 wz = (thedata<1e-100)
 if np.sum(wz)>0:
  thedata[wz] = thedata[wz]+np.min(thedata[np.logical_not(wz)])/1000.0
 thedata=log10(thedata)

timestamps=slice[1]
timemax=size(timestamps)
starttime=0

if (options.starttime>0):
 starttime=int(options.starttime)
if (options.ntime>0):
 timemax=starttime+int(options.ntime)

if (type(options.min)!=type(None)):
 levelmin=float(options.min)
else:
 levelmin=min(thedata)
if (type(options.max)!=type(None)):
 levelmax=float(options.max)
else:
 levelmax=max(thedata)

#levelmin=0
#levelmax=3
myLevels=linspace(levelmin,levelmax,num=200)

if coords=='cartesian':
 x=grid.x[3:-3]
 y=grid.y[3:-3]
 x2d,y2d=meshgrid(x,y)
else:
 radius=grid.x[3:-3]
 if coords=='spherical':
  if options.extension=='xz':
   phi=grid.z[3:-3]
  else:
   phi=grid.y[3:-3]
 elif coords=='cylindric':
  phi=grid.y[3:-3]
 nradius=size(radius)
 nphi=size(phi)
 radius2d=zeros((nphi,nradius))
 phi2d=zeros((nphi,nradius))
 blank=ones_like(slice[0][0])
 radius2d=blank*radius[newaxis,:]
 phi2d=blank*phi[:,newaxis]
 x2d=radius2d*cos(phi2d)
 y2d=radius2d*sin(phi2d)

if options.last:
 starttime=timemax-1

uniqtime = str(time())

def worker():
 slicedata=thedata[time]
 fig=plt.figure()#figsize=(8,6))
 ax=fig.add_subplot(111)
 myContour=ax.pcolor(x2d,y2d,slicedata,vmin=levelmin,vmax=levelmax)#,levels=myLevels,rasterized=True) #,cmap=plt.cm.hot)

 cb=plt.colorbar(myContour)
 if options.log:
  cb.set_label("log10(%s)"%field)
 else:
  cb.set_label(field)

 title="Time: %s"%str(timestamps[time])
 plt.title(title)
 plt.xlabel("X")
 plt.ylabel("Y")
 outname="%s/slice%s.%s.%s.%s.png"%(outdir,str(extension).upper(),field,str(time).zfill(int(log10(timemax)+1)),uniqtime)
 fig.savefig(outname,dpi=options.dpi)
 print outname

for time in xrange(starttime,timemax):
 stdout.write("\rPlotting slice %s/%s"%(str(time-starttime+1),str(timemax-starttime)))
 stdout.flush()
 if __name__=='__main__':
  proc=mp.Process(target=worker)
  proc.daemon=True
  proc.start()
  proc.join()


print

script = r"""
uniqtime=%s
cd ./animation_files

nfile=$(ls slice*${uniqtime}.png | wc -l)

dig=$(ls slice*${uniqtime}.png | sort -n | head -1 | perl -pe "s|^.*?\..*?\.||" | perl -pe "s|\..*?\.png\n||")
ndig=${#dig}
filebase=$(ls slice*${uniqtime}.png | sort -n | head -1 | sed "s/${dig}/%s${ndig}d/")
ffmpeg -framerate 5 -i $filebase -r 20 -g 20 -q 1 animation_${uniqtime}.mpeg
"""%(uniqtime,'%')
system("bash -c '%s'" % script)
